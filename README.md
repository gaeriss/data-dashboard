# Data Dashboard


## Description
Here is a server side script to store and vizualize operational data from a water-mill. Those scripts result in [dataviz.energie-de-nantes.fr](dataviz.energie-de-nantes.fr).

This project stores every 2 to 3 seconds operational data into a CSV file on the server (data.csv), thanks to [datagetter.py](./datagetter.py)

Every 2 minutes, a [calculator.py](./calculator.py) script aggregates data.csv in a 5min-aggreated-samples data files.

It creates a web server and displays a data dashbard thanks to [main.py](./main.py) using [streamlit library](https://streamlit.io/) each time a user connects to [dataviz.energie-de-nantes.fr](dataviz.energie-de-nantes.fr)

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
For any help when your project (if you are a non profit organisation that understands [nantes en commun](https://www.nantesencommun.org/) purpose, contact us @ contact@energie-de-nantes.fr

## Contributing
Contribution is welcomed! Create a branch and we'l see :)

## Authors and acknowledgment
A big thank to [Ilyes](https://gitlab.com/Torilen) and [Hugo Simon](https://gitlab.com/Mougrouff)

## License
This is an open source project from [nantes en commun](https://www.nantesencommun.org/), any commercial use of this code, (i.e. reproduction, integration, inspiratio) will be denounced and is strictly disapproved by authors. If you are an association (non profit organization), contact us !
