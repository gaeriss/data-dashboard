import pandas as pd
import os
import requests
import time
from datetime import datetime
import json
from utils import send_message
import gc

trigger_notif = {"ouverture": False, "fermeture": False}

#pip install pandas requests json streamlit plotly

if not os.path.exists('data.csv'):
    pd.DataFrame([], columns=["time", "bp_fermer_vanne_travail1", "bp_ouvrir_vanne_travail1", "compteur_energie_active_real1"
        , "niveau_eau", "puissance_generatrice1", "puissance_generatrice1_kw"
        , "puissance_generatrice_maximale1", "puissance_generatrice_moyenne1", "vitesse_generatrice1"]).to_csv('data.csv')

cookies = {
    'IDALToken': 'a0ad55f006f09fa993ce0484afd8b404',
}

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate',
    'X-Requested-With': 'XMLHttpRequest',
    'Connection': 'keep-alive',
    'Referer': 'http://88.161.139.215:33333/filature/web/web/pages/m3_page3.html',
}

def login():
    print("RELOGIN")
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0',
        'Accept': 'text/html, */*; q=0.01',
        'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
        'Accept-Encoding': 'gzip, deflate',
        'X-Requested-With': 'XMLHttpRequest',
        'Connection': 'keep-alive',
        'Referer': 'http://88.161.139.215:33333/public/_weblogin.html?pg=_m3_page3',
    }

    params = (
        ('username', 'admin'),
        ('password', 'moulinfilature44'),
        ('_', str(int(time.time()))),
    )

    response = requests.get('http://88.161.139.215:33333/cgi/login', headers=headers, params=params, cookies=cookies)
    print("LOGIN LOG - {}".format(response.content))
    if "TOO_MANY_USERS" in str(response.content):
        print("TOO_MANY_USERS")
        return "-1"
    return str(response.content).split("IDALToken=")[1][:-1]

while True:
    params = (
            ('n', '1'),
            ('gr1', 'DesktopProduction_temps_reel@$GroupSubscrWgt@$0'),
            ('_', str(int(time.time()))),
        )

    params2 = (
        ('n', '1'),
        ('gr1', 'DesktopTemplatePage1@$GroupSubscrWgt@$0'),
        ('_', str(int(time.time()))),
    )

    try:
        response = requests.get('http://88.161.139.215:33333/cgi/readGroups.json',
                                headers=headers, params=params, cookies=cookies)
    except requests.exceptions.ConnectionError:
        print('Connection error, retrying...')
        time.sleep(1)
        # Skip the rest of the while loop
        continue
    except requests.exceptions.HTTPError:
        print('HTTP error, retrying...')
        time.sleep(1)
        # Skip the rest of the while loop
        continue
    except requests.exceptions.Timeout:
        print('Timeout error, retrying...')
        time.sleep(1)
        # Skip the rest of the while loop
        continue
    except requests.exceptions.TooManyRedirects:
        print('Too many redirects, retrying...')
        time.sleep(1)
        # Skip the rest of the while loop
        continue
    except Exception as e:
        print('Unknown error : \n'+repr(e)+'\n')
        print('Retrying...')
        time.sleep(1)
        # Skip the rest of the while loop
        continue

    if "NEED_LOGIN" in str(response.content):
        try:
            cookies["IDALToken"] = login()
            time.sleep(1)
        except:
            pass
    else:
        #response = requests.get('http://88.161.139.215:33333/cgi/readGroups.json', headers=headers, params=params, cookies=cookies)
        try:
            d = dict()
            d['time'] = datetime.now()
            for i in json.loads(response.content)['gs'][0]['tgs']:
                d[i['name'].split('//')[1]] = [i['value']]

            pd.DataFrame(d).to_csv('data.csv', mode='a', header=False)

            #notification system
            if d["bp_fermer_vanne_travail1"][0] == 1 and not trigger_notif["fermeture"]:
                send_message("Je suis en train de me fermer car on me l'a demandé", "angreviers")
                trigger_notif["fermeture"] = True

            if d["bp_ouvrir_vanne_travail1"][0] == 1 and not trigger_notif["ouverture"]:
                send_message("Je suis en train de m'ouvrir car on me l'a demandé", "angreviers")
                trigger_notif["ouverture"] = True

            if d["bp_fermer_vanne_travail1"][0] == 0:
                trigger_notif["fermeture"] = False

            if d["bp_ouvrir_vanne_travail1"][0] == 0:
                trigger_notif["ouverture"] = False

            print("GET DATA LOG - {}".format(response.content))
            gc.collect()
        except:
            print(str(response.content))
            pass
        time.sleep(2)
