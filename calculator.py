import pandas as pd
from datetime import datetime
import time
import os 
 
######## CALCULATOR SCRIPT ######## 
# Purpose is to calculate every 2 min some basic data elements we need in the UI, and output results in some csv files.
# Output is written in the "data" folder. 

time_format = '%Y-%m-%d %H:%M:%S.%f'
time_dict = {}
while True:
    start_time = datetime.now()
    print ("START TIME : " + start_time.strftime(time_format))
    # Read full data file
    data = pd.read_csv('data.csv', engine="pyarrow")
    print ("DATA SIZE : " + str(len(data))) 
    time_dict['load_time'] = datetime.now() - start_time
    last_time = datetime.now()
    
    # Clean up negative values
    data.loc[:,"puissance_generatrice1"] = data["puissance_generatrice1"][data["puissance_generatrice1"] >= 0 ]
    # Indexing data
    data.index = pd.to_datetime(data['time'])
    time_dict['clean_index'] = datetime.now() - start_time
    last_time = datetime.now()
    
    # create data directory if necessary
    os.makedirs('data', exist_ok=True) 

    # Write data per 5 min
    grouper_per_5min = data.groupby(pd.Grouper(freq='5min'))
    data_per_5min = grouper_per_5min.mean(numeric_only=True)
    # Get maximum power over 5 min
    max_power_5min = grouper_per_5min['puissance_generatrice1'].max()
    data_per_5min.to_csv('data/data_per_5min.csv')
    max_power_5min.to_csv('data/max_power_5min.csv')
    time_dict['5min_writing'] = datetime.now() - last_time
    last_time = datetime.now()
    
    # Write counter per month
    grouper_per_month = data.groupby(pd.Grouper(freq='M'))['compteur_energie_active_real1']
    counter_per_month = grouper_per_month.max()-grouper_per_month.min()
    counter_per_month.to_csv('data/counter_per_month.csv')
    time_dict['counter_per_month_writing'] = datetime.now() - last_time
    last_time = datetime.now()
    
    # Write counter per week
    grouper_per_week = data.groupby(pd.Grouper(freq='W'))['compteur_energie_active_real1']
    counter_per_week = grouper_per_week.max()-grouper_per_week.min()
    counter_per_week.to_csv('data/counter_per_week.csv')
    time_dict['counter_per_week_writing'] = datetime.now() - last_time
    last_time = datetime.now()

    print ("PROFILER :",time_dict)
    print ("TOTAL RUN TIME : " + str(datetime.now()-start_time))
    print()
    
    # wait 2 min - at power 15kW, kWh counter would change every 4 min.
    time.sleep(120)