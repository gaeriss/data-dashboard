import os
import time
import requests
import pandas as pd

def send_message(message, moulin):
    if moulin == "angreviers":
        TOKEN = "5676255093:AAGMlYuH3x8zn6JlaX0MayGWFUk2CUFfn5I"
        chat_id = "-719433659"
        url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={message}"
        requests.get(url).json()
        
def get_kWh_in_month(data,year,month):

    if type(data.index) != pd.DatetimeIndex:
        data.index = pd.to_datetime(data['time'])
    data_per_month = data.groupby(pd.grouper(freq='m'))['compteur_energie_active_real1']
    counter = data_per_month.max()-data_per_month.min()
    
    month_date = str(year)+"-"+str(month)
    return counter[month_date][0]
    
