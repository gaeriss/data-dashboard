import time
import pandas as pd
from datetime import datetime
import streamlit as st
import plotly.express as px
from sys import getsizeof

data = []

    # Press the green button in the gutter to run the script.
print("Running...")
st.set_page_config(
        page_title="Turbine Angreviers Temps Réel",
        page_icon="✅",
        layout="wide",
    )

st.title("Data Dashboard Angreviers")
moulin_filter = st.selectbox("Sélectionnez le moulin", ["Angreviers"])
col1, col2, col3 = st.columns(3)
with col1:
    with open("data.csv", "r") as file:
        st.download_button(label="Télécharger les données en CSV", data = file,file_name='historique_production.csv', mime='text/csv',key=1)
with col2:
    st.markdown("[📈 Explorer les données](./Explorer_les_données) :link:")

with col3:
    st.markdown("[🌊 Vigicrue](https://www.vigicrues.gouv.fr/niv3-station.php?CdEntVigiCru=9&CdStationHydro=M730242011&GrdSerie=H&ZoomInitial=3) :link:")


placeholder = st.empty()

while True:
    data = pd.read_csv("data.csv", chunksize=100000)
    data = pd.concat(data)
    counter_per_month = pd.read_csv('data/counter_per_month.csv')
    counter_per_month.index = pd.to_datetime(counter_per_month['time'])
    data = data.tail(200)

    if "placeholder" in st.session_state:
        del st.session_state["placeholder"]
    with placeholder.container():

        kpi1, kpi2, kpi3, kpi4, kpi6 = st.columns(5)
        #print(d)
        # fill in those three columns with respective metrics or KPIs
        kpi1.metric(
                    label="Puissance (W)",
                    value=round(data.iloc[-1]["puissance_generatrice1"]),
                    delta=round(data.iloc[-1]["puissance_generatrice1"])-round(data.iloc[-2]["puissance_generatrice1"])
                )

        kpi2.metric(
                    label="Puissance moyenne (W - 10min)",
                    value=round(data["puissance_generatrice1"].mean()),
                )

        kWh_produced_in_month = counter_per_month.loc[str(datetime.now().year)+"-"+str(datetime.now().month)]["compteur_energie_active_real1"][0]

        kpi3.metric(
                    label= "Compteur "+ datetime.now().strftime("%B")+ " (kWh)",
                    value=round(kWh_produced_in_month),
                )
        kpi4.metric(
                    label="Vitesse rotation (tr/min)",
                    value=round(data.iloc[-1]["vitesse_generatrice1"]),
                    delta=round(data.iloc[-1]["vitesse_generatrice1"])-round(data.iloc[-2]["vitesse_generatrice1"])
                )

        kpi6.metric(
                    label="Niveau d'eau (mm)",
                    value=round(data.iloc[-1]["niveau_eau"]),
                    delta=round(data.iloc[-1]["niveau_eau"])-round(data.iloc[-2]["niveau_eau"])
                )
        fig_col1, fig_col2 = st.columns(2)

        with fig_col1:
            st.markdown("### Historique production")
            fig = px.line(data_frame=data, x="time", y="puissance_generatrice1")
            st.write(fig)

        with fig_col2:
            st.markdown("### Évolution niveau d'eau")
            fig2 = px.line(data_frame=data, x="time", y="niveau_eau")
            st.write(fig2)

        st.markdown("### Data")
        st.dataframe(data.sort_values(by="time", ascending=False))
        # st.line_chart(pd.read_csv('data.csv')[["puissance_generatrice1"]])
        print("data {} | placeholder {} | kp1 {} | kp2 {} | kp3 {} | kp4 {} | kp6 {} | fig1 {} | fig2 {} | st {}".format(getsizeof(data), getsizeof(placeholder), getsizeof(kpi1), getsizeof(kpi2), getsizeof(kpi3), getsizeof(kpi4), getsizeof(kpi6), getsizeof(fig_col1), getsizeof(fig_col2), getsizeof(st)))
        time.sleep(5)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
