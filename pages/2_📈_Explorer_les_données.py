import pandas as pd
from datetime import datetime
import streamlit as st
import plotly.express as px
from dateutil import relativedelta
import utils

st.markdown("# 📈 Explorer les données")
st.sidebar.markdown("# 📈 Explorer les données")

    
def get_kWh_in_range(data,start_date,end_date):
    if not type(start_date)== str:
        start_date = datetime.strftime(start_date,"%Y-%m-%d")
        end_date = datetime.strftime(end_date,"%Y-%m-%d")
    
    first_kWh_in_range = data.loc[start_date]['compteur_energie_active_real1'].iloc[0]
    last_kWh_in_range = data.loc[end_date]['compteur_energie_active_real1'].iloc[0]
    return last_kWh_in_range - first_kWh_in_range

# # Load data
# data = pd.read_csv('data.csv', engine="pyarrow")

# # Clean up negative values of puissance_generatrice1
# data.loc[:,"puissance_generatrice1"] = data["puissance_generatrice1"][data["puissance_generatrice1"] >= 0 ]
# # Group data
# data.index = pd.to_datetime(data['time'])

data_per_5min = pd.read_csv('data/data_per_5min.csv')
data_per_5min.index = pd.to_datetime(data_per_5min['time'])
data_per_5min_perMonth = data_per_5min.groupby(pd.Grouper(freq='M'))
max_power_5min = pd.read_csv('data/max_power_5min.csv')
max_power_5min.index = pd.to_datetime(max_power_5min['time'])
counter_per_month = pd.read_csv('data/counter_per_month.csv')
counter_per_month.index = pd.to_datetime(counter_per_month['time'])

# Plot counter per week
st.header("Compteur par semaine")
st.sidebar.header("Compteur par semaine")
#counter_per_week = data.groupby(pd.Grouper(freq='W')).max()['compteur_energie_active_real1']-data.groupby(pd.Grouper(freq='W')).min()['compteur_energie_active_real1']
counter_per_week = pd.read_csv('data/counter_per_week.csv')
counter_per_week.index = pd.to_datetime(counter_per_week['time'])
fig3 = px.bar(data_frame=counter_per_week, y="compteur_energie_active_real1",color='compteur_energie_active_real1',labels={'index':'Semaine (dimanche)','compteur_energie_active_real1':'Compteur (kWh)'})
st.write(fig3)

#####  Month KPIs  ######

st.header(" Indicateurs par Mois")
st.sidebar.header(" Indicateurs par Mois")

month_list = pd.date_range('2022-11-01',datetime.now(), freq='MS').strftime("%Y-%b").tolist()

month_filter = st.selectbox("Sélectionnez le mois", month_list,len(month_list)-1)

placeholder = st.empty()
with placeholder.container():
    kpi1, kpi2, kpi3 = st.columns(3)

    kpi1.metric(
            label="Compteur (kWh)",
            value=round(counter_per_month.loc[month_filter]['compteur_energie_active_real1'][0])
        )
   
    kpi2.metric(
            label="Puissance moyenne (W)",
            value=round(data_per_5min.groupby(pd.Grouper(freq='M')).mean()["puissance_generatrice1"][month_filter][0])
        )

    kpi3.metric(
            label="Puissance max (W)",
            value=round(max_power_5min.groupby(pd.Grouper(freq='M')).max()["puissance_generatrice1"][month_filter][0])
        )
        
st.header(" Indicateurs par Période")
st.sidebar.header(" Indicateurs par Période")

##### Period KPIs  ######

date_selection = st.date_input("Selectionnez une période de calcul", value=(datetime.now()-relativedelta.relativedelta(weeks=2),datetime.now()), min_value=datetime.strptime("2022-11-27","%Y-%m-%d"), max_value=datetime.now(), key=None) 

if len(date_selection)>1:
    placeholder2 = st.empty()
    with placeholder2.container():
        kpi1, kpi2, kpi3 = st.columns(3)

        
        
        start_date = datetime.strftime(date_selection[0],"%Y-%m-%d")
        end_date = datetime.strftime(date_selection[1],"%Y-%m-%d")
        
        kpi1.metric(
                label="Compteur (kWh)",
                value=round(data_per_5min.sort_index()[start_date:end_date]['compteur_energie_active_real1'].max()-data_per_5min.sort_index()[start_date:end_date]['compteur_energie_active_real1'].min())
               
            )

        kpi2.metric(
                label="Puissance moyenne (W)",
                value=round(data_per_5min.sort_index()[start_date:end_date]["puissance_generatrice1"].mean())
            )

        kpi3.metric(
                label="Puissance max (W)",
                value=round(max_power_5min.sort_index()[start_date:end_date]["puissance_generatrice1"].max())
            )

    # Plot period           
    fig2 = px.line(data_frame=data_per_5min[start_date:end_date], y="puissance_generatrice1", markers=True)
    st.write(fig2)
